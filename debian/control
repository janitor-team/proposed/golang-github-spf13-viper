Source: golang-github-spf13-viper
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>,
           Tim Potter <tpot@hpe.com>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang (>= 1.39~),
               golang-any,
               golang-github-fsnotify-fsnotify-dev,
               golang-github-hashicorp-hcl-dev,
               golang-github-magiconair-properties-dev (>= 1.8.1~),
               golang-github-mitchellh-mapstructure-dev,
               golang-github-pelletier-go-toml-dev,
               golang-github-spf13-afero-dev,
               golang-github-spf13-cast-dev,
               golang-github-spf13-jwalterweatherman-dev,
               golang-github-spf13-pflag-dev,
               golang-github-stretchr-testify-dev,
               golang-github-subosito-gotenv-dev (>= 1.2.0~),
               golang-gopkg-ini.v1-dev (>= 1.51.0~),
               golang-gopkg-yaml.v2-dev (>= 2.2.4~),
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-spf13-viper
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-spf13-viper.git
Homepage: https://github.com/spf13/viper
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/spf13/viper

Package: golang-github-spf13-viper-dev
Architecture: all
Depends: golang-github-fsnotify-fsnotify-dev,
         golang-github-hashicorp-hcl-dev,
         golang-github-magiconair-properties-dev (>= 1.8.1~),
         golang-github-mitchellh-mapstructure-dev,
         golang-github-pelletier-go-toml-dev,
         golang-github-spf13-afero-dev,
         golang-github-spf13-cast-dev,
         golang-github-spf13-jwalterweatherman-dev,
         golang-github-spf13-pflag-dev,
         golang-github-subosito-gotenv-dev (>= 1.2.0~),
         golang-gopkg-ini.v1-dev (>= 1.51.0~),
         golang-gopkg-yaml.v2-dev (>= 2.2.4~),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Go configuration with fangs
 Viper is a complete configuration solution for Go applications
 including 12-Factor apps.  It is designed to work within an application,
 and can handle all types of configuration needs and formats.
 It supports:
 .
  * setting defaults
  * reading from JSON, TOML, YAML, HCL, and Java properties config files
  * live watching and re-reading of config files (optional)
  * reading from environment variables
  * reading from remote config systems (etcd or Consul), and watching changes
  * reading from command line flags
  * reading from buffer
  * setting explicit values
 .
 Viper can be thought of as a registry for all of your applications
 configuration needs.
